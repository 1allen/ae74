<?php
const API_URL = 'https://api.import.io/store/connector/defdb588-ea77-400e-8876-e13e933f973a/_query';
const API_KEY = 'df9339bce19249088fc72f064800cc68605a5afb9ad62d71b7f122d3027710d0d6956ac5b84f0eda852f9361e0220717962764fb0a2361c63322a29a528126db22f1a338e51d0d9e3335ce2b32dc8733';
require '../vendor/autoload.php';

// Prepare app
$app = new \Slim\Slim(array(
    'templates.path' => '../templates',
));

// Create monolog logger and store logger in container as singleton 
// (Singleton resources retrieve the same log resource definition each time)
$app->container->singleton('log', function () {
    $log = new \Monolog\Logger('slim-skeleton');
    $log->pushHandler(new \Monolog\Handler\StreamHandler('../logs/app.log', \Monolog\Logger::DEBUG));
    return $log;
});

// Prepare view
$app->view(new \Slim\Views\Twig());
$app->view->parserOptions = array(
    'charset' => 'utf-8',
    'cache' => realpath('../templates/cache'),
    'auto_reload' => true,
    'strict_variables' => false,
    'autoescape' => true
);

$app->view->parserExtensions = array(
    new \Slim\Views\TwigExtension(),
    new \JSW\Twig\TwigExtension()
);

// Define routes
$app->get('/', function () use ($app) {

    $todayParams = [
        'input' => 'webpage/url:http://aeroport-74.ru/ajax/ttable.php?day=today',
        '_apikey' => API_KEY,
    ];
    $tomorrowParams = [
        'input' => 'webpage/url:http://aeroport-74.ru/ajax/ttable.php?day=tomorrow',
        '_apikey' => API_KEY,
    ];

//	Unirest\Request::verifyPeer(false);

    $todayData = \Unirest\Request::get(API_URL, [], $todayParams);

    if (!isset($todayData->body->results)) {
        $app->error('error receiving data');
    }

    $tomorrowData = \Unirest\Request::get(API_URL, [], $tomorrowParams);

    $todayRes = $todayData->body->results;
    $tomorrowRes = $tomorrowData->body->results;

    usort($todayRes, function ($a, $b) {
        return (isset($a->time_est) ? (int)$a->time_est : 0) > (isset($b->time_est) ? (int)$b->time_est : 0);
    });
    usort($tomorrowRes, function ($a, $b) {
        return (isset($a->time_est) ? (int)$a->time_est : 0) > (isset($b->time_est) ? (int)$b->time_est : 0);
    });

    $res = $todayRes + ['route' => '—'] + $tomorrowRes;
    //	var_dump($res);die();

    // Render index view
    $app->render('index.html.twig', ['data' => $res]);
});
// Define 404 template
$app->notFound(function () use ($app) {
    $app->render('404.html.twig');
});

// Run app
$app->run();